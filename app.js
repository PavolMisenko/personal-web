const express = require('express');
const app = express();

//app.get('/', (req, res) => res.send('Hello World!')) //Mapovanie webovej aplikácia. Vlastne metódou get mi pošle na urlčku "/" pošle. 
//app.get('/xxx', (req, res) => {
//    const time = new Date()
//    res.send()
//})
//app.get('/hello/:name/', function (req, res) {
//    res.send(req.params.name) //Z tohto som dostal to, že ak načítam tú "get" url-čku tak dostanem to čo som tam napísal namiesto mena
//})
// tie zapoznamkovane casti som si tu nechal preto, nech v buducnosti vidim ako sa niektore veci robili

app.listen(3000, () => console.log('Example app listening on port 3000!')) //Listen robí to, že čaká na to či muniekto niečo pošle (napríklad url) a ak pošle tak ono to spatne odošle preddefinované veci

app.use('/Home', express.static('Home')) //Použije akutálny router a na adrese static mi použije funkciu "express.static"
app.use('/Projects', express.static('Projects'))
app.use('/About_me', express.static('About_me'))
app.use('/Contact', express.static('Contact'))
app.use('/Education', express.static('Education'))